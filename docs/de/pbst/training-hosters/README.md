# Befehle von Tier 4's und Trainern

:::warning Diese befehle sind meistens für Trainings. 
Viele der hier aufgeführten Befehle werden von Assistenten, Special Defense (Tier 4) und Trainern benutzt.   
Jede PB Einrichtung benutzt Kronos, wodurch die Benutzung von Befehlen unter dem Rang Tier 4 nicht möglich sein wird.

Wenn du in eine Training hostest oder ein Assistent in einem bist. Das Training ist nur in **ENGLISCH**. Also spreche immer flüssig oder wenigstens verständlich. 
:::

:::danger Nicht nur kopieren und einfügen Bitte merke dass diese Befehle ** BEISPIELE ** sind.
 Du musst sie je nachdem wie du das Training entwickelt verändern.   
Teste deine Befehle bevor du sie im ** EIGENTLICHEN ** Training benutzt... 
:::

## Mögliche Parameter
- `me` -> du.
- `player-name` ->  ein Spieler den du auswählst.
- `all` -> jeder in dem Server (also auch du und zuschauer)
- `%team-name` ->Ein Team auswählen (**Beispiel**: `;add %Winners Win 1`).
- `nonadmins` -> Jeder Spieler der kein Administrator ist (Höchstwarscheinlich jeder unter dem Rang Tier 4. **ACHTUNG**: Das heißt das auch Zuschauer eingeschlossen sind).
- `radius-<distance>` -> Jeder in dem Radius den du angibst. (**Beispiel**: `;team radius-20 %Winners`)
- `others` -> Dies wählt jeden aus, außer dich selbst (Wenn du Assistenten oder Zuschauer hast ,werden diese auch ausgewählt)

## Welche Befehle sind meistens in Trainings benutzt?
- `;fly | ;god`  
Dieser Befehl erlaubt es dir zu fliegen und macht dich unbesiegbar (Du kannst keinen Schaden nehmen)
- `;team <argument> <team>`  
Setzt einen Spieler in das Team das du angegeben und mit den unteren Befehlen kreiert hast.
- `;newteam Host/Assistants <color>`  
Wenn du ein Training veranstaltest erstellt dieser Befehl ein Team für den Veranstalter und die Assistenten
- `;newteam Observers <color>`  
Mache ein Team für Zuschauer. **Merke dir:**: Diese bekommen nie Punkte.)
- `;newteam Winners <color>`  
Ein Team um die Sieger zu bestimmen und ihnen Siege zuzuschreiben. (Sodass du nicht jeden mit einem Sieg belohnst.)
- `;newteam Security <color>`  
Dies ist das Standard Team, indem sich jeder befinden sollte (Außer der Host, Zuschauer und Assistenten). ![img](https://i.imgur.com/vLwSyFK.png)

- `;addstat Wins`  
Fügt eine Statistik hinzu, damit du sehen kannst wie viele Siege alle Teilnehmer haben, und diese für das Log nutzen kannst.
- `;m <message>`  
Dies ist eine Ankündigung, die jeder auf seinem Bildschirm mit der Standard Roblox Linie sehen kann. ![img](https://i.imgur.com/PcjEr9v.png)
- `;n <message>`  
Eine kleine Box die jeder auf seinem Bildschirm sehen kann. ![img](https://i.imgur.com/rZKvc03.png)
- `;h <message>` ![img](https://i.imgur.com/YXRuuJB.png) Eine Nachricht, die an den oberen Rand des Bildschirms geschickt wird.
- `;add <argument> Wins 1`  
Gibt jemandem oder einem Team einen Sieg (Spieler brauchen kein %, aber Teams brauchen es.)
- `;team radius-<distance> <Team>`  
Teamt Leute um dich herum in ein Team das ist sehr nützlich, um zum Siege vergeben.
- `;give <argument> <tool>`  
Gibt Spielern ein Item, welches sich in der Toolbox befindet.
- `;sword <argument>`  
Ein kurzer Befehl der Spielern Schwerter gibt.
- `;removetools <argument>`  
Entfernt die Items der ausgewählten Spieler.
- `;viewtools <argument>`  
Erlaubt es dir, die Items zu sehen, welch die ausgewählten Personen bei sich tragen.
- `!importalias <!alias> <Command>`  
Benutze diesen Befehl, um ein Alias hinzuzufügen. Diese dienen dazu, Befehle zusammenzufassen. (Beispiel: **!importalias !shirtme !shirt [146487695](https://www.roblox.com/catalog/146487695/Security-Vest-Specops-PB)**)

## Vorschläge für Aliase

 1. Öffne Kronos indem du auf das PBST Logo in der rechten unteren ecke klickst.
 2. Öffne den Tab "Aliases"
 3. Drücke "Add" am unteren Rand des Fensters.
 4. Fülle das Feld **Alias:** mit dem Alias den du benutzen möchtest.
 5. Gebe einen Befehl weiter unter an.

**`;assist`**:
- `;team <arg1> Host/Assistants | ;fly <arg1> | ;god <arg1> | ;sword <arg1>` Gibt einer Person Assistenten Befehle und platziert sie in dem Host/Assistant Team. Trainer können auch zusätzliche Befehle verwenden, um den Alias zu verbessern:
- `;team <arg1> Host/Assistants | ;admin <arg1> | ;loadout <arg1> Special` Gibt der Person Temp Mod für das Training und Tier 4 (Special Defense) Waffen.

**`;observe`**:
- `;team <arg1> Obs | ;fly <arg1> | ;god <arg1>`   
Platziert jemanden in das Observer team und gibt ihnen Zuschauer befehle.  
![img](https://i.imgur.com/imuV43Y.png)

**`;s2vote`**:
- `;vote nonadmins,-%ob Sword_Fight,Guns,Juggernaut,Bombs 20 What activity shall we do?` Nützlich für den Host, um einfach eine Arena innerhalb von Sektor 2 auszuwählen.

**`;noreturn`**:
- `;lobbydoors off | ;teleporters off`  
Sperrt die Lobby ab, damit niemand aus ihr heraus kommt.

**`;bafk`**:
- `;bring <arg1> | ;afk <arg1>` Bringt die Person zu dir und AFK't sie.

**`;flygod`**:
- `;fly <arg1> | ;god<arg1>` Dies erlaubt dem ausgewählten Spieler zu fliegen und keinen Schaden zu nehmen.

**`;bjail`**:
- `;bring <arg1> | ;jail <arg1>` Bringt die Person zu dir und sperrt sie ein.

**`;win20`**:
- `;team radius-20 %Winners
Teamt die Personen im Radius von 20 in das Team "Winners"`

**`;twin`**:
- `;add %Winners Wins 1
Gibt den Gewinnern einen Gewinn.`

**`;unteamwin`**:
- `;unteam %Winners`

**`;trainingsetup`**:
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on | ;addstat Strikes` 

Um den Alias weiter zu entwickeln,  sind mehrere Versionen davon angegeben.


**Credits an *MarusiFyren* und *EquilibriumCurse* für die Hilfe bei der Erstellung dieser Seite, und Mariusi für einige der Bilder, die auf dieser Seite benutzt wurden.**