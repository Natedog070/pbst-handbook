
# PBST Handbuch
## Uniform
Die offiziellen PBST-Uniformen finden Sie im Shop [hier](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Auch wenn du keine Robux hast, sind die meisten Anlagen mit Uniform-Gebern ausgestattet, um sie dort zu holen. Es wird trotzdem empfohlen, der Bequemlichkeit halber, eine Uniform zu kaufen.

Du musst beim Patrouillieren einer Pinewood Anlage eine offizielle PBST-Uniform tragen.

### Standorte der Uniform Geber
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) Vom Hauptspawn durch die Lobby, den Fahrstuhl hoch, (folge der "Security Sector + Cafe" Linie) und dann im PBST-Raum an der rechten Seite.
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) Vom Hauptspawn in den Raum mit der Bezeichnung „PB SEC“, dann zu dem Flur zwischen dem Baton-Geber und der Kaffeemaschine, im Umkleideraum auf der linken Seite.
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) 4. Stock, von den Aufzügen zu den Glastüren links, dann in die blauen Türen auf der linken Seite.
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility) Auf der rechten Seite des Spawngebäudes
* [PBST Aktivitätszentrum:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) In dem Flur der Lobby

### Tragen von Uniformen
Das Tragen einer PET Feuer oder Hazmat Uniform ist erlaubt, aber nur wenn eine offizielle PBST Uniform darunter getragen wird. Die Medical Uniform ist nicht erlaubt, da im Gegensatz zu den anderen beiden, diese das Shirt & die Hose wechselt, und es dadurch unmöglich macht, eine PBST Uniform damit zu tragen.

::: warning NOTIZ
 Ab dem 21. März 2020, hat eine Abstimmung der Trainer stattgefunden, welche festlegt, dass PBST-Offiziere nun die PET-Hazmat-Uniform und die PET-Feuer-Uniform verwenden dürfen, allerdings  **NUR ** wenn sie eine offizielle PBST-Uniform damit tragen. Das bedeutet, dass du die Anzüge unter allen Umständen frei verwenden kannst**_solange du die PBST-Uniform darüber trägst_**.  
:::

## Im Dienst
Du musst eine offizielle PBST-Uniform tragen, um als im Dienst angesehen zu werden. Dein Rangzeichen muss ebenfalls an und auf "Security" gesetzt sein (der Standard), wenn du allerdings nur das Rangabzeichen anhast, bist du nicht im Dienst.

Wenn dieses auf **TMS** oder **PET** gesetzt ist, führe bitte den Befehl `!setgroup PBST` aus

Als PBST Mitglied im Dienst bist du verpflichtet die Anlage vor jeglicher Bedrohung zu schützen. Wenn du evakuieren musst, versuche dein Bestes, den anderen zu helfen.

Um außer Dienst zu gehen, musst du den Sicherheitsraum betreten und deine Uniform und deine PBST-Waffen entfernen. Setze dich selbst zurück falls nötig. Es wird dringend empfohlen, dass du auch dein Security Rangabzeichen entfernst, indem Sie den Befehl “`!setgroup [PBST/TMS/PET/Group Name]`", um zu einer neutralen Gruppe wie PET, PB oder einer anderen Pinewood Gruppe zu wechseln, oder benutze “`!ranktag off`” Befehl, um den Rangabzeichen vollständig auszuschalten.

### Werkzeuge anderer Abteilungen
Wenn Sie für PBST im Dienst sind, dürfen Sie keine Werkzeuge oder Waffen verwenden, die mit Rängen in anderen Gruppen verbunden sind, wie TMS & PET, dies gilt auch umgekehrt. (PET und TMS dürfen Waffen von PBST nicht verwenden)

Wenn du jemanden siehst, der Werkzeuge nimmt, der für diese Gruppe nicht im Dienst ist Mache ein Bild oder nimm sie auf und melde sie einem hochrangigen Mitglied.

### Waffen
Als höherer Rank erhältst du Waffen, diese sind tödlich und können jemanden in der Anlage töten, diese Waffen dürfen in keiner Weise missbraucht werden (zum Beispiel Spawnkillen, Töten von zufälligen Menschen ohne Gründe usw.). Wenn du jemals jemanden siehst, der so etwas tut, melde dies bitte einem PIA-Offizier)

Außerdem, in [PBSTTF](https://www.roblox.com/games/298521066/PBST-Training-Facility) oder jeder anderen Anlage mit einem tödlichen Waffengeber, der eingeschaltet werden kann, darfst du keinem dieser Waffen geben. Die einzige Ausnahme ist, wenn ein T4+ dich danach fragt.

## Pinewood Computer Core (PBCC)
Der [Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) ist der wichtigste Ort zu patrouillieren. Dein Hauptziel hier ist zu verhindern, dass der Kern schmilzt oder friert. Die folgende Tabelle zeigt Ihnen, wie unterschiedliche Einstellungen die Temperatur beeinflussen. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228" />

PBCC enthält auch verschiedene „Katastrophen“, von denen einige PBST benötigen, um die Besucher zu evakuieren. Im Falle einer Plasma-Welle, eines Erdbebens oder einer "Strahlenflut", bringe die Menschen, die in Gefahr sind, in Sicherheit.

If your efforts fail and a meltdown does occur, you can go to the Emergency Coolant at Sector G to try to rescue the core. Use the Security Code to get in (the code that’s announced in-game). Keep the e-coolant in balance until the timer hits 0.

This requires effort from help with the other members of PBST. You have to work together to keep the emergency coolent between 70% and 80%. When you do this successfully and the procentages are between 70 and 80, the core has a 50% chance of returning to a state of 3000 degrees. If you fail this, you're doomed to the core.

### Temperatur Rechner
To calculate the TPS (temperture per second) I've added a calculator: <Calculator />

## Waffennutzung
Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts. Be sure to have your uniform on when taking these weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty. **PBST weapons may never be used to cause a melt-or freezedown.**

You have to give **two** warnings to people before you can kill them. If a visitor has been killed after three warnings and still returns, you can kill that person *without warning*. On-duty PBST members also have to follow these rules when using non-PBST weapons like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned

In case of emergency, a Tier 4 or higher may grant you permission to restrict a room to **PBST only**. In that case, you can kill anyone who tries to enter and only let *on-duty PBST* in.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. You can, for example, restrict rooms without needing permission, though excessive room restriction may count as "mass random killing".

Mass random killing and spawn killing are always forbidden, use the `!call` command to call PIA to your assistance if someone is mass random killing or spawn killing.

## Pinewood Emergency Team
The Pinewood Emergency Team (PET) is set up as an emergency response group to a few types of emergencies at Pinewood facilities. They often help PBST in our mission to keep them safe. You can switch between being on-duty for PBST or PET by using the `!setgroup` command.

### Abteilungen
PET has 3 subdivisions:
 - Medizin
 - Feuerwehr
 - Hazmat

### PET-Uniformen
Each have their own uniform. The Fire & Hazmat uniforms allow you to wear a PBST uniform underneath it, the Medical uniform doesn’t (see the [Uniform](#Uniform) chapter).


Please read [Weapons](#Weapons) for more informations about using

Read the PET handbook for more details on how PET works: [PET Handbook](https://devforum.roblox.com/t/pet-handbook-29-01-2020/398123)

## Rule Breakers
Visitors who break a small rule are usually given a warning. You have to give two warnings to people before you can kill them. If a visitor has been killed after three warnings and still returns, you can kill that person without warning.

If a visitor is attempting to melt or freeze the core by changing one of the core controls, give a warning. If their response is hostile, you can kill them.

Anyone flying a train is to be killed on sight, anyone flying a vehicle is to be tased and said vehicle is to be removed.

If a Security member is found breaking a rule in the handbook, alert them of their wrongdoing and give them a warning. If said Security member doesn’t listen or actually uses their weapons irresponsibly, kill them.

If a Tier is being abusive with their weapons, send a PIA call to deal with them.

## The Mayhem Syndicate (TMS)
The Mayhem Syndicate (**TMS**) is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. You can recognize them by the red rank tag. They often join a game in a raid, and when this happens you are advised to call for backup.

The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

### An Überfällen teilnehmen
If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to the side for either TMS or PBST in a raid might get you set on KoS (Kill on Sight) by the other side.

## Töten auf Sicht (KoS)
Any member of TMS is to be killed on sight. Mutants are KoS as well. **PBST members may not become mutants while on duty.** On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Training
::: warning NOTE: 
Instructions in training are given in English, so understanding English is required. 
:::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Training Rules**:
* Höre auf den Host und folge seinen Anweisungen.
* Nimm keine Ausrüstung, der Host gibt dir das, was du für die Aktivitäten brauchst.
* Trage im Training immer eine PBST-Uniform.
* Trainer können die Berechtigung zum Sprechen (PTS) aktivieren, welches bedeutet, dass du bestraft wirst, wenn du redest. Sage "`;pts request`", um Erlaubnis zu bekommen, wenn dies aktiviert ist.
* Einige Trainings sind als Disziplinartraining gekennzeichnet, sie sind strenger und sind stärker auf Disziplin ausgerichtet. Du wirst aus dem Training geworfen, wenn du dich falsch verhältst.
* Höchstens einmal pro Woche wird ein Hardcore Training veranstaltet. In einem solchen Training führt selbst der kleinste Fehler zu einem Rausschmiss.  Wenn du bis zum Ende „überlebst“ , kannst du bis zu 20 Punkte verdienen.

More information about points can be found [here](../ranks-and-ranking-up/)

## Ränge
You will receive the next Tier rank once you reach the required amount of points. With your new rank comes a new loadout in Pinewood facilities: Tier 1 gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With Tier 2 you receive all the former plus a rifle. At Tier 3 you receive all the former plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

::: tip Recent change! 
As of `02/28/2020`. Tier **1** evaluations are now a thing. It you want to go from `Cadet` -> `Tier 1`. You need to complete an evaluation.

You can read more about it [here](../ranks-and-ranking-up/#tier-evaluations). 
:::

Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the `!call PBST` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at training, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

## Rang 4 (auch Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

Like Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host training with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainer
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Erinnerungen an alle PBST-Mitglieder
  * Bitte halte dich die ganze Zeit an die [ROBLOX Gemeinschaftsregeln](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Sei respektvoll gegenüber deinen Mitspielern, handel nicht so, als wärst du der Chef des Spiels.
  * Folgen Sie den Anweisungen von jedem mit einem höheren Rang.

::: warning NOTE: 
These rules can be changed at any time. So please check every now and then for a update on the handbook. 
:::
